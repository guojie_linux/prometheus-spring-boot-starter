package top.codef.httpclient;

import java.util.Map;

import feign.Body;
import feign.Headers;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;
import top.codef.pojos.dingding.DingDingNotice;
import top.codef.pojos.dingding.DingDingResult;

public interface DingDingClientFeign {

	@RequestLine("POST /send?access_token={accessToken}")
	@Headers("Content-Type: application/json; charset=utf-8")
	@Body("{body}")
	DingDingResult post(@Param("accessToken") String accessToken, DingDingNotice body,
			@QueryMap Map<String, Object> map);
}
